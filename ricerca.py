import requests
import random
from bs4 import BeautifulSoup 
import traceback
import concurrent.futures
import csv
from random import randrange
import sys

#il file csv potrebbe contenere campi molto grandi quindi conviene aumentare il field_size_limit
csv.field_size_limit(sys.maxsize)

#Prima di tutto vado a riempire tutte queste strutture dati con il contenuto presente nei file csv creati
#dall'esecuzione dello scraper in precedenza.Poi vado a fare una ricerca se una certa parola chiave è presente
titoli_eleak=[]
links_eleak=[]
time_eleak=[]
infos_eleak=[]

titoli_zone=[]
links_zone=[]
data_proprietario_zone=[]

titoli_father=[]
links_father=[]
data_father=[]

with open('eleaks.csv','r') as csv_file:
	csv_reader=csv.reader(csv_file)
	#ogni line è una riga di eleak.csv
	for line in csv_reader:
		titoli_eleak.append(line[0])
		links_eleak.append(line[1])
		time_eleak.append(line[2])
		infos_eleak.append(line[3])

with open('hackingfather.csv','r') as csv_file:
	csv_reader=csv.reader(csv_file)
	#ogni line è una riga di hackingfather.csv
	for line in csv_reader:
		titoli_father.append(line[0])
		links_father.append(line[1])
		data_father.append(line[2])
	

with open('leakzone.csv','r') as csv_file:
	csv_reader=csv.reader(csv_file)
	#ogni line è una riga di leakzone.csv
	for line in csv_reader:
		titoli_zone.append(line[0])
		links_zone.append(line[1])
		data_proprietario_zone.append(line[2])


while True:
	keyword=input('Inserire &&& per fermare l esecuzione del programma oppure una keyword da rilevare(Risultati inseriti nel file result_keyword.csv) ')
	if keyword=='&&&':
		break
	nomefile='result_'+keyword+'.csv'
	csv_file=open(nomefile,'w')
	csv_writer=csv.writer(csv_file)
	csv_writer.writerow(['titolo','link','data','info'])
	i=0
	for titolo in titoli_eleak:
		if keyword in titolo:
   			final_link=links_eleak[i]
   			data=time_eleak[i]
   			info=infos_eleak[i]
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for info in infos_eleak:
		if keyword in info:
   			final_link=links_eleak[i]
   			data=time_eleak[i]
   			titolo=titoli_eleak[i]
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for titolo in titoli_zone:
		if keyword in titolo:
   			final_link=links_zone[i]
   			data=data_proprietario_zone[i]
   			info=''
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for titolo in titoli_father:
		if keyword in titolo:
   			final_link=links_father[i]
   			data=data_father[i]
   			info=''
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	
