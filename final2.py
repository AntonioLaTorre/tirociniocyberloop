import requests
import random
from bs4 import BeautifulSoup 
import traceback
import concurrent.futures
import csv
from random import randrange
import sys

#il file csv potrebbe contenere campi molto grandi quindi conviene aumentare il field_size_limit
csv.field_size_limit(sys.maxsize)

def get_free_proxies():
	#fare lo scraping del sito per ricavare indirizzi ip
	#vedere se funzionano con google e caratterizzati da HTTPs

	#c'è un sito web:"httpbin.org/ip" che ritorna il nostro indirizzo ip attuale come risposta
	#Si può usare questo sito quindi per controllare se il proprio IP è valido e se quindi usabile
	url = "https://free-proxy-list.net/"
	#request and grab content
	soup=BeautifulSoup(requests.get(url).content,'html.parser')
	#per immagazzinare proxies
	proxies=[]
	#per immagazzinare se è funzionante con Google e caratterizzato da HTTPs
	for row in soup.find("table", attrs={"class":"table table-striped table-bordered"}).find_all("tr")[1:]:
		tds=row.find_all("td")
		try:
			ip=tds[0].text.strip()
			port=tds[1].text.strip()
			#vogliamo utilizzare indirizzi IP che configurano HTTPS
			#e funzionano in google quindi faremo un controllo 
			#tds[5] indica se funziona con google e tds[6] se proxy HTTPs
			google=tds[5].text.strip()
			https=tds[6].text.strip()
			if ((google!='no') and  (https!='no')):
				proxies.append(str(ip)+":"+str(port))
			else:
				continue
		except IndexError:
			continue
	return proxies
def get_user_agent():
	f=open("user_agent.txt")
	text=f.read()
	f.close()
	##nel file user_agent.txt il delimeter tra ogni user_agent è "$$$"
	list_useragent=text.split('$$$')
	##con funzione randrange prendo indice casuale che può essere in un range che va da 0 
	#alla lunghezza della lista
	i_rand=randrange(len(list_useragent))
	useragent=list_useragent[i_rand]
	return useragent
	
url="http://httpbin.org/ip"
proxies = get_free_proxies()
user_agent=get_user_agent()

#nel nostro caso ci serve solo un indirizzo quindi appena uno risulta funzionante
#facciamo terminare il ciclo
t=0
for i in range(len(proxies)):
	 proxy=proxies[i]
	 if t==0:
		 try:
		 	response=requests.get(url,proxies={"http":proxy,"https":proxy})
		 	ip=proxy.split(':')[0]
		 	port=proxy.split(':')[1]
		 	t=t+1
		 except:
		 	continue
#caso molto raro ma sempre possibile è che nessun proxy risulti valido quindi per risolvere questo problema
#si controlla che t non sia uguale a 0 .Se t==0 il programma termina
if t==0:
	print('nessun proxy valido,riprovare tra qualche minuto')
	exit()



#leaks inseriti in un file csv
csv_file=open('eleaks.csv','w')
csv_writer=csv.writer(csv_file)
csv_writer.writerow(['titolo','link','time','info'])

#proxy inserito a mano grazie allo script indirizziIP2.py
url_eleak='https://eleaks.to/forums/databases.31/'
titoli_eleak=[]
links_eleak=[]
time_eleak=[]
infos_eleak=[]

def extract_link(link):
	info=''
	databases=''
	try:
		html_text = requests.get(link,proxies={'http': ip,'http': ip},headers={"User-Agent":user_agent}).text
		soup=BeautifulSoup(html_text,'lxml')
	except:
		pass		
	try:
		div_=soup.find('div',{'class':'bbWrapper'})
		info=div_.text
	except:
		pass
	try:
		databases=div_.find('div',{'class':'bbCodeCode'}).code.text
	except:
		pass
	x=info+databases
	#faccio questo per posizionare nella posizione corretta le info del leak
	index=links_eleak.index(link)
	infos_eleak.insert(index,x)
	

#ritorna soup per permettere nel ciclo for di ottenere il soup della pagina attuale
def extract_from_page_eleak(url):
	html_text = requests.get(url,proxies={'http': ip,'http': ip},headers={"User-Agent":user_agent}).text
	#print(r.status_code)
	soup=BeautifulSoup(html_text,'lxml')
	date=[]
	#variabile che servirà per verificare che un titolo è gia presente
	tit_presente=''
	#ottengo prima la lista di tutte le date dei leaks della pagina
	#utilizzo variabili i per far corrispondere il leak e la sua date
	i=0
	for html_data in soup.find_all('div',class_='structItem-minor'):
		try:
			ul_data=html_data.find('ul',{'class':'structItem-parts'})
			li_data=ul_data.find('li',{'class':'structItem-startDate'})
			data=li_data.find('time',{'class':'u-dt'}).text
			date.append(data)
		except:	
			pass
	#ciclo per inserire titoli,link e data(ottenuta con il ciclo precedente) nel file csv
	for html_titolo in soup.find_all('div',class_='structItem-title'):
		data=''
		titolo= html_titolo.a.text
		link=html_titolo.a['href']
		final_link='https://eleaks.to'+link
		data=date[i]
		#per evitare che ci siano duplicati all'interno delle liste controllo che il titolo non è gia presente
		for tit_already in titoli_eleak:
			if titolo == tit_already:
				tit_presente='presente'
		if tit_presente != 'presente':
			titoli_eleak.append(titolo)
			links_eleak.append(final_link)
			time_eleak.append(data)
		i=i+1

	
	return soup
		    
	
soup=extract_from_page_eleak(url_eleak)
#in html_numpage mi ritrovo il codice html della sezione riguardante le pagine
html_numpage=soup.find('ul',{'class':'pageNav-main'}).find_all('li',{'class':'pageNav-page'})
#in num_page mi ritrovo il numero delle pagine
num_page=int(html_numpage[-1].find('a').text)

#itero da pagina 2 fino a num_page
for i in range(2,num_page+1):
	#creo l'url per ottenere la pagina successiva
	url_eleak='https://eleaks.to/forums/databases.31/'+'page-'+str(i)
	soup=extract_from_page_eleak(url_eleak)

with concurrent.futures.ThreadPoolExecutor() as executor:
	executor.map(extract_link,links_eleak)

for j in range(0,len(infos_eleak)):
	info=''
	titolo=titoli_eleak[j]
	final_link=links_eleak[j]
	data=time_eleak[j]
	info=infos_eleak[j]
	csv_writer.writerow([titolo,final_link,data,info])


csv_file=open('leakzone.csv','w')
csv_writer=csv.writer(csv_file)
csv_writer.writerow(['titolo','link','data e proprietario'])

url_zone='https://leakzone.net/Forum-Dumps-databases'

titoli_zone=[]
links_zone=[]
data_proprietario_zone=[]
#ritorna soup per permettere nel ciclo for di ottenere il soup della pagina attuale
def extract_from_page_zone(url):
	html_text = requests.get(url,proxies={'http': ip,'http': ip},headers={"User-Agent":user_agent}).text
	#print(r.status_code)
	soup=BeautifulSoup(html_text,'lxml')
	date=[]
	#ottengo prima la lista di tutte le date dei leaks della pagina
	#utilizzo variabili i per far corrispondere il leak e la sua date

	#variabile che servirà per verificare che un titolo è gia presente
	tit_presente=''
	i=0
	for html_data in soup.find_all('span',class_='lastpost smalltext'):
		try:
			data=html_data.text
			date.append(data)
		except:	
			pass
	#ciclo per inserire titoli,link e data(ottenuta con il ciclo precedente) nel file csv
	for html_titolo in soup.find_all('tr',class_='inline_row'):
		try:
			span_titolo=html_titolo.find('span',class_='subject_new')
			titolo= span_titolo.find('a').text
			link=span_titolo.a['href']
			final_link='https://leakzone.net'+link
			##in questo caso insieme alla data vi è anche l'username del proprietario
			#per evitare che ci siano duplicati all'interno delle liste controllo che il titolo non è gia presente
			for tit_already in titoli_zone:
				if titolo == tit_already:
					tit_presente='presente'
			if tit_presente != 'presente':
				titoli_zone.append(titolo)
				links_zone.append(final_link)
				data=date[i]
				data_proprietario_zone.append(data)
		except:
			pass
		i=i+1

		#titoli.append(titolo)
		#link_titoli.append(final_link)
	return soup
		    
	
soup=extract_from_page_zone(url_zone)
span_page=soup.find('span',class_='pagination')
#in num_page mi ritrovo il numero delle pagine
num_page=int(span_page.find('a',class_='pagination_last').text)

#itero da pagina 2 fino a num_page
for i in range(2,num_page+1):
	#creo l'url per ottenere la pagina successiva
	url_zone='https://leakzone.net/Forum-Dumps-databases?'+'page='+str(i)
	soup=extract_from_page_zone(url_zone)

for j in range(0,len(titoli_zone)):
	titolo=titoli_zone[j]
	final_link=links_zone[j]
	data=data_proprietario_zone[j]
	csv_writer.writerow([titolo,final_link,data])




csv_file=open('hackingfather.csv','w')
csv_writer=csv.writer(csv_file)
csv_writer.writerow(['titolo','link','data'])

url_father='https://hackingfather.com/forums/databases.22/'

titoli_father=[]
links_father=[]
data_father=[]

#ritorna soup per permettere nel ciclo for di ottenere il soup della pagina attuale
def extract_from_page_father(url):
	html_text = requests.get(url,proxies={'http': ip,'http': ip},headers={"User-Agent":user_agent}).text
	soup=BeautifulSoup(html_text,'lxml')
	date=[]
	#ottengo prima la lista di tutte le date dei leaks della pagina
	#utilizzo variabili i per far corrispondere il leak e la sua date

	#variabile che servirà per verificare che un titolo è gia presente
	tit_presente=''
	i=0
	for html_data in soup.find_all('div',class_='structItem-minor'):
		try:
			ul_data=html_data.find('ul',{'class':'structItem-parts'})
			li_data=ul_data.find('li',{'class':'structItem-startDate'})
			data=li_data.find('time',{'class':'u-dt'}).text
			date.append(data)
		except:	
			pass
	#ciclo per inserire titoli,link e data(ottenuta con il ciclo precedente) nel file csv
	for html_titolo in soup.find_all('div',class_='structItem-title'):
		#ho intenzione di prendere il secondo(titolo) a in quei casi in cui c'è ne sono di piu'
		
		a_html=html_titolo.find_all('a')
		if len(a_html)==2:
			titolo= a_html[1].text
			link=a_html[1]['href']
		else:
			titolo= html_titolo.a.text
			link=html_titolo.a['href']
		final_link='https://hackingfather.com/'+link
		data=date[i]
		#per evitare che ci siano duplicati all'interno delle liste controllo che il titolo non è gia presente
		for tit_already in titoli_father:
			if titolo == tit_already:
				tit_presente='presente'
		if tit_presente != 'presente':
			titoli_father.append(titolo)
			links_father.append(final_link)
			data_father.append(data)


		i=i+1

		#titoli.append(titolo)
		#link_titoli.append(final_link)
	return soup
		    
	
soup=extract_from_page_father(url_father)
#in html_numpage mi ritrovo il codice html della sezione riguardante le pagine
html_numpage=soup.find('ul',{'class':'pageNav-main'}).find_all('li',{'class':'pageNav-page'})
#in num_page mi ritrovo il numero delle pagine
num_page=int(html_numpage[-1].find('a').text)

#itero da pagina 2 fino a num_page
for i in range(2,num_page+1):
	#creo l'url per ottenere la pagina successiva
	url_father='https://hackingfather.com/forums/databases.22/'+'page-'+str(i)
	soup=extract_from_page_father(url_father)

for j in range(0,len(titoli_father)):
	titolo=titoli_father[j]
	final_link=links_father[j]
	data=data_father[j]
	csv_writer.writerow([titolo,final_link,data])

while True:
	keyword=input('Inserire &&& per fermare lesecuzione del programma oppure una keyword da rilevare(Risultati inseriti nel file result_keyword.csv) ')
	if keyword=='&&&':
		break
	nomefile='result_'+keyword+'.csv'
	csv_file=open(nomefile,'w')
	csv_writer=csv.writer(csv_file)
	csv_writer.writerow(['titolo','link','data','info'])
	i=0
	for titolo in titoli_eleak:
		if keyword in titolo:
   			final_link=links_eleak[i]
   			data=time_eleak[i]
   			info=infos_eleak[i]
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for info in infos_eleak:
		if keyword in info:
   			final_link=links_eleak[i]
   			data=time_eleak[i]
   			titolo=titoli_eleak[i]
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for titolo in titoli_zone:
		if keyword in titolo:
   			final_link=links_zone[i]
   			data=data_proprietario_zone[i]
   			info=''
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	for titolo in titoli_father:
		if keyword in titolo:
   			final_link=links_father[i]
   			data=data_father[i]
   			info=''
   			csv_writer.writerow([titolo,final_link,data,info])
		i=i+1
	i=0
	