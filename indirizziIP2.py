#fare lo scraping del sito per ricavare indirizzi ip
#vedere se funzionano con google e caratterizzati da HTTPs

#c'è un sito web:"httpbin.org/ip" che ritorna il nostro indirizzo ip attuale come risposta
#Si può usare questo sito quindi per controllare se il proprio IP è valido e se quindi usabile
#in una rotazione di indirizzi ip

#proxy usabili inseriti in un file csv
import requests
import random
from bs4 import BeautifulSoup as bs
import traceback
import concurrent.futures


#sito per ricavare indirizzi ip:"https://free-proxy-list.net/"
def get_free_proxies():
	url = "https://free-proxy-list.net/"
	#request and grab content
	soup=bs(requests.get(url).content,'html.parser')
	#per immagazzinare proxies
	proxies=[]
	#per immagazzinare se è funzionante con Google e caratterizzato da HTTPs
	for row in soup.find("table", attrs={"class":"table table-striped table-bordered"}).find_all("tr")[1:]:
		tds=row.find_all("td")
		try:
			ip=tds[0].text.strip()
			port=tds[1].text.strip()
			#vogliamo utilizzare indirizzi IP che configurano HTTPS
			#e funzionano in google quindi faremo un controllo 
			#tds[5] indica se funziona con google e tds[6] se proxy HTTPs
			google=tds[5].text.strip()
			https=tds[6].text.strip()
			if ((google!='no') and  (https!='no')):
				proxies.append(str(ip)+":"+str(port))
			else:
				continue
		except IndexError:
			continue
	return proxies

url="http://httpbin.org/ip"
proxies = get_free_proxies()

#nel nostro caso ci serve solo un indirizzo alla volta quindi appena uno risulta funzionante
#facciamo terminare il programma
t=0
ip=''
for i in range(len(proxies)):
	 proxy=proxies[i]
	 if t==0:
		 try:
		 	response=requests.get(url,proxies={"http":proxy,"https":proxy})
		 	ip=proxy.split(':')[0]
		 	port=proxy.split(':')[1]
		 	print(ip)
		 	t=t+1
		 except:
		 	continue
